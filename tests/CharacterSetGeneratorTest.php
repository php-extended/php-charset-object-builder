<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\CharacterSetGenerator;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

/**
 * CharacterSetGeneratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Charset\CharacterSetGenerator
 *
 * @internal
 *
 * @small
 */
class CharacterSetGeneratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CharacterSetGenerator
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CharacterSetGenerator(new NullLogger());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}

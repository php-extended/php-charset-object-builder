<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-object-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Charset;

use Psr\Log\LoggerInterface;
use RuntimeException;
use Stringable;

/**
 * CharacterSetGenerator class file.
 * 
 * This class is made to generate the CharacterSet class file from the 
 * character sets data csv file that is get back from the unicode consortium
 * website.
 * 
 * @author Anastaszor
 */
class CharacterSetGenerator implements Stringable
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The code template for the character set classes.
	 * 
	 * @var string
	 */
	protected string $_baseClassCode;
	
	/**
	 * The code template for the test classes.
	 * 
	 * @var string
	 */
	protected string $_baseTestCode;
	
	/**
	 * @var array<integer, string>
	 */
	protected array $_idMap = [];
	
	/**
	 * @var array<string, string>
	 */
	protected array $_aliasMap = [];
	
	/**
	 * @var array<string, string>
	 */
	protected array $_classMap = [];
	
	/**
	 * The path to the character set file.
	 * 
	 * @var string
	 */
	private string $_dataFilePath = __DIR__.'/../data/character-sets.csv';
	
	/**
	 * The path to the directory of the php-charset-object library.
	 * 
	 * @var string
	 */
	private string $_charsetObjectLibPath;
	
	/**
	 * Builds a new CharacterSetGenerator with the given base template.
	 * 
	 * @param LoggerInterface $logger
	 * @throws RuntimeException
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
		
		$baseClassCode = \file_get_contents(\dirname(__DIR__).'/data/CharacterSet.php.stub');
		if(false === $baseClassCode)
		{
			throw new RuntimeException('Failed to find base class code.');
		}
		$this->_baseClassCode = $baseClassCode;
		
		$baseTestCode = \file_get_contents(\dirname(__DIR__).'/data/CharacterSetTest.php.stub');
		if(false === $baseTestCode)
		{
			throw new RuntimeException('Failed to find base test code.');
		}
		$this->_baseTestCode = $baseTestCode;
		
		$this->_charsetObjectLibPath = \dirname(__DIR__, 2).'/php-charset-object';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Regenerates all the character sets data from the csv file. This will
	 * overwrite the current files present in this directory.
	 * 
	 * @throws RuntimeException for any problem
	 */
	public function regenerateAll() : void
	{
		$this->regenerateAllClasses();
		$this->regenerateDataFile('id_map', $this->_idMap);
		$this->regenerateDataFile('alias_map', $this->_aliasMap);
		$this->regenerateDataFile('class_map', $this->_classMap);
		$this->regenerateReadme();
		
		$this->_logger->info('END OF SCRIPT');
	}
	
	/**
	 * Regenerates the given file from the given data.
	 * 
	 * @param string $fileName
	 * @param array<integer|string, string> $values
	 * @throws RuntimeException
	 */
	public function regenerateDataFile(string $fileName, array $values) : void
	{
		$classMapContents = "<?php declare(strict_types=1);\n\nnamespace PhpExtended\\Charset;\n\nreturn [\n\t".\implode("\n\t", $values)."\n];\n";
		$filePath = $this->_charsetObjectLibPath.'/data/'.$fileName.'.php';
		$res = \file_put_contents($filePath, $classMapContents);
		if(false === $res)
		{
			throw new RuntimeException(\strtr('Failed to write at path {path}', ['{path}' => $filePath]));
		}
		$this->_logger->info('WRITING '.$filePath);
	}
	
	/**
	 * Regenerates all the classes based on the csv file.
	 * 
	 * @throws RuntimeException
	 */
	public function regenerateAllClasses() : void
	{
		$file = \fopen($this->_dataFilePath, 'r');
		if(false === $file)
		{
			throw new RuntimeException('Failed to open file at '.$this->_dataFilePath);
		}
		
		$line = 1;
		\fgetcsv($file, 1000, ',', '"'); // skip first line
		
		while(false !== ($data = \fgetcsv($file, 1000, ',', '"')))
		{
			$line++;
			if(empty($data))
			{
				continue;
			}
			
			if(6 > \count($data))
			{
				throw new RuntimeException(\strtr('Failed to read csv data, not enough columns on line {line}', ['{line}' => $line]));
			}
			
			$preferredName = (string) ($data[0] ?? '');
			$name = (string) ($data[1] ?? '');
			$mibenum = $data[2];
			$source = $data[3];
			$reference = $data[4];
			$aliases = $data[5];
			
			if('' === $preferredName)
			{
				$preferredName = $name;
			}
			
			if('' === $preferredName)
			{
				throw new RuntimeException(\strtr('Failed to read csv data, empty preferred name on line {line}', ['{line}' => $line]));
			}
			
			if('' === $name)
			{
				throw new RuntimeException(\strtr('Failed to read csv data, empty name on line {line}', ['{line}' => $line]));
			}
			
			if(!\is_numeric($mibenum))
			{
				throw new RuntimeException(\strtr('Failed to read csv data, mibenum is not a number on line {line}', ['{line}' => $line]));
			}
			
			$this->regenerateClass($preferredName, $name, (int) $mibenum, $source, $reference, $aliases);
		}
	}
	
	/**
	 * Writes the class code for the given row of the csv file.
	 * 
	 * @param string $preferredName
	 * @param string $name
	 * @param integer $mibenum
	 * @param ?string $source
	 * @param ?string $reference
	 * @param ?string $aliases
	 * @throws RuntimeException if something fails
	 */
	public function regenerateClass(string $preferredName, string $name, int $mibenum, ?string $source, ?string $reference, ?string $aliases) : void
	{
		$classname = (string) \mb_strtoupper((string) \preg_replace('#\\W+#', '_', $preferredName));
		$this->_idMap[] = "'".$name."' => ".$classname.'::class,';
		$this->_classMap[$preferredName] = $classname.'::class => 1,';
		
		/**
		 * PHPSTAN : Parameter #2 $callback of function array_filter expects
		 * callable(null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array>, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array>): bool, 'strlen' given.
		 */
		/** @phpstan-ignore-next-line array_filter+strlen */
		$aliases = \array_filter(\array_map('trim', \explode("\n", (string) $aliases)), 'strlen');
		$aliasesStr = '// no aliases available';
		if(!empty($aliases))
		{
			$aliasesStr = "'".\implode("',\n\t\t\t'", $aliases)."',";
			
			foreach($aliases as $alias)
			{
				$this->_aliasMap[$alias] = "'".$name."' => ".$classname.'::class,';
			}
		}
		
		$rfcNumber = 'null';
		$matches = [];
		if(\preg_match('#RFC(\\d+)#', (string) $reference, $matches))
		{
			/** @phpstan-ignore-next-line */
			if(isset($matches[1]))
			{
				$rfcNumber = $matches[1];
			}
		}
		
		$sourceUrl = 'null';
		$matches = [];
		if(\preg_match('#\\[(http.+)\\]#ui', (string) $source, $matches))
		{
			/** @phpstan-ignore-next-line */
			if(isset($matches[1]))
			{
				$sourceUrl = "'".\strtr($matches[1], ["'" => "\\'"])."'";
			}
		}
		
		foreach([
			$this->_baseClassCode => $this->_charsetObjectLibPath.'/src/'.$classname.'.php',
			$this->_baseTestCode => $this->_charsetObjectLibPath.'/test/'.$classname.'Test.php',
		] as $sourcePath => $destPath)
		{
			$code = \strtr($sourcePath, [
				'{{class}}' => __CLASS__,
				'{{charsetname}}' => $preferredName,
				'{{classname}}' => $classname,
				'{{fullname}}' => $name,
				'{{mibenum}}' => $mibenum,
				'{{sourcecomment}}' => \strlen((string) $source) === 0 ? 'null' : "'".\strtr((string) $source, ["'" => "\\'"])."'",
				'{{sourceurl}}' => $sourceUrl,
				'{{rfcnb}}' => $rfcNumber,
				'{{aliases}}' => $aliasesStr,
			]);
			
			$res = \file_put_contents($destPath, $code);
			if(false === $res)
			{
				throw new RuntimeException('Failed to write '.$classname.' class');
			}
		}
		
		$this->_logger->info('Writing charset '.$preferredName.' to class '.$classname);
	}
	
	/**
	 * Regenerates the readme file.
	 * 
	 * @throws RuntimeException
	 */
	public function regenerateReadme() : void
	{
		$readme = \file_get_contents($this->_charsetObjectLibPath.'/README.md');
		if(false === $readme)
		{
			throw new RuntimeException('Failed to read readme.');
		}
		
		$pos1 = \mb_strpos($readme, '## Implemented Charsets Status');
		if(false === $pos1)
		{
			throw new RuntimeException('Failed to load implemented charset statuses from readme.');
		}
		
		$pos2 = \mb_strpos($readme, '## License', $pos1);
		if(false === $pos2)
		{
			throw new RuntimeException('Failed to load license from readme.');
		}
		
		\ksort($this->_classMap);
		$newReadme = (string) \mb_substr($readme, 0, $pos1);
		$newReadme .= "## Implemented Charsets Status\n\n\n";
		$longestcharset = 0;
		$longestclassname = 0;
		
		foreach($this->_classMap as $charsetname => $classname)
		{
			$classname = \str_replace('::class => 1,', '', $classname);
			$longestcharset = (int) \max($longestcharset, \mb_strlen((string) $charsetname));
			$longestclassname = (int) \max($longestclassname, \mb_strlen($classname));
		}
		
		foreach($this->_classMap as $charsetname => $classname)
		{
			$classname = \str_replace('::class => 1,', '', $classname);
			$newReadme .= '| '.\str_pad((string) $charsetname, $longestcharset, ' ', \STR_PAD_RIGHT).' | '.\str_pad($classname, $longestclassname, ' ', \STR_PAD_RIGHT)." |\n";
		}
		$newReadme .= "\n\n\n\n".((string) \mb_substr($readme, $pos2));
		
		$res = \file_put_contents(__DIR__.'/../README.md', $newReadme);
		if(false === $res)
		{
			throw new RuntimeException('Failed to write README');
		}
		
		$this->_logger->info('Writing README');
	}
	
}

<?php declare(strict_types=1);

use PhpExtended\Charset\CharacterSetGenerator;
use PhpExtended\Logger\BasicConsoleLogger;
use Psr\Log\LogLevel;

/**
 * This script is to regenerate the whole character set collection. 
 * (This needs require from composer).
 * 
 * Usage : php regenerate.php
 * 
 * @author : Anastaszor
 */

$autoload = __DIR__.'/vendor/autoload.php';
if(!is_file($autoload))
{
	throw new \RuntimeException('Composer must be runned first.');
}
require $autoload;

$logger = new BasicConsoleLogger();
$logger->setVerbosityLevel(3);
$generator = new CharacterSetGenerator($logger);
$generator->regenerateAll();

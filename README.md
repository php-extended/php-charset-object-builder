# php-extended/php-charset-object

A library that implements the php-charset-interface package.

This library implements [that database from the iana](https://www.iana.org/assignments/character-sets/character-sets.xhtml).

![coverage](https://gitlab.com/php-extended/php-charset-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-charset-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-charset-object ^8`


## Basic Usage

This library may be used the following way :

- List all character sets

```php

use PhpExtended\Charset\CharacterSet;

foreach(CharacterSet::values() as $characterSet)
{
	// do something here
}

```

- Search for a specific character set (the values have to be the name or the
full name of the character set)

```php

use PhpExtended\Charset\CharacterSetReference;

/* @var $utf8 \PhpExtended\Charset\UTF_8 */
$utf8 = CharacterSetReference::lookup('UTF-8');

```

You may also search by less common values (The values have to be in the aliases
of the character set or in the name or full name).

```php

/* @var $latin1 \PhpExtended\Charset\ISO_8859_1 */
$latin1 = CharacterSetReference::lookupAlias('latin1');

```


## Implemented Charsets Status


| ANSI_X3.110-1983                            | ANSI_X3_110_1983                            |
| ASMO_449                                    | ASMO_449                                    |
| Adobe-Standard-Encoding                     | ADOBE_STANDARD_ENCODING                     |
| Adobe-Symbol-Encoding                       | ADOBE_SYMBOL_ENCODING                       |
| Amiga-1251                                  | AMIGA_1251                                  |
| BOCU-1                                      | BOCU_1                                      |
| BRF                                         | BRF                                         |
| BS_4730                                     | BS_4730                                     |
| BS_viewdata                                 | BS_VIEWDATA                                 |
| Big5                                        | BIG5                                        |
| Big5-HKSCS                                  | BIG5_HKSCS                                  |
| CESU-8                                      | CESU_8                                      |
| CP50220                                     | CP50220                                     |
| CP51932                                     | CP51932                                     |
| CSA_Z243.4-1985-1                           | CSA_Z243_4_1985_1                           |
| CSA_Z243.4-1985-2                           | CSA_Z243_4_1985_2                           |
| CSA_Z243.4-1985-gr                          | CSA_Z243_4_1985_GR                          |
| CSN_369103                                  | CSN_369103                                  |
| DEC-MCS                                     | DEC_MCS                                     |
| DIN_66003                                   | DIN_66003                                   |
| DS_2089                                     | DS_2089                                     |
| EBCDIC-AT-DE                                | EBCDIC_AT_DE                                |
| EBCDIC-AT-DE-A                              | EBCDIC_AT_DE_A                              |
| EBCDIC-CA-FR                                | EBCDIC_CA_FR                                |
| EBCDIC-DK-NO                                | EBCDIC_DK_NO                                |
| EBCDIC-DK-NO-A                              | EBCDIC_DK_NO_A                              |
| EBCDIC-ES                                   | EBCDIC_ES                                   |
| EBCDIC-ES-A                                 | EBCDIC_ES_A                                 |
| EBCDIC-ES-S                                 | EBCDIC_ES_S                                 |
| EBCDIC-FI-SE                                | EBCDIC_FI_SE                                |
| EBCDIC-FI-SE-A                              | EBCDIC_FI_SE_A                              |
| EBCDIC-FR                                   | EBCDIC_FR                                   |
| EBCDIC-IT                                   | EBCDIC_IT                                   |
| EBCDIC-PT                                   | EBCDIC_PT                                   |
| EBCDIC-UK                                   | EBCDIC_UK                                   |
| EBCDIC-US                                   | EBCDIC_US                                   |
| ECMA-cyrillic                               | ECMA_CYRILLIC                               |
| ES                                          | ES                                          |
| ES2                                         | ES2                                         |
| EUC-JP                                      | EUC_JP                                      |
| EUC-KR                                      | EUC_KR                                      |
| Extended_UNIX_Code_Fixed_Width_for_Japanese | EXTENDED_UNIX_CODE_FIXED_WIDTH_FOR_JAPANESE |
| GB18030                                     | GB18030                                     |
| GB2312                                      | GB2312                                      |
| GBK                                         | GBK                                         |
| GB_1988-80                                  | GB_1988_80                                  |
| GB_2312-80                                  | GB_2312_80                                  |
| GOST_19768-74                               | GOST_19768_74                               |
| HP-DeskTop                                  | HP_DESKTOP                                  |
| HP-Legal                                    | HP_LEGAL                                    |
| HP-Math8                                    | HP_MATH8                                    |
| HP-Pi-font                                  | HP_PI_FONT                                  |
| HZ-GB-2312                                  | HZ_GB_2312                                  |
| IBM-Symbols                                 | IBM_SYMBOLS                                 |
| IBM-Thai                                    | IBM_THAI                                    |
| IBM00858                                    | IBM00858                                    |
| IBM00924                                    | IBM00924                                    |
| IBM01140                                    | IBM01140                                    |
| IBM01141                                    | IBM01141                                    |
| IBM01142                                    | IBM01142                                    |
| IBM01143                                    | IBM01143                                    |
| IBM01144                                    | IBM01144                                    |
| IBM01145                                    | IBM01145                                    |
| IBM01146                                    | IBM01146                                    |
| IBM01147                                    | IBM01147                                    |
| IBM01148                                    | IBM01148                                    |
| IBM01149                                    | IBM01149                                    |
| IBM037                                      | IBM037                                      |
| IBM038                                      | IBM038                                      |
| IBM1026                                     | IBM1026                                     |
| IBM1047                                     | IBM1047                                     |
| IBM273                                      | IBM273                                      |
| IBM274                                      | IBM274                                      |
| IBM275                                      | IBM275                                      |
| IBM277                                      | IBM277                                      |
| IBM278                                      | IBM278                                      |
| IBM280                                      | IBM280                                      |
| IBM281                                      | IBM281                                      |
| IBM284                                      | IBM284                                      |
| IBM285                                      | IBM285                                      |
| IBM290                                      | IBM290                                      |
| IBM297                                      | IBM297                                      |
| IBM420                                      | IBM420                                      |
| IBM423                                      | IBM423                                      |
| IBM424                                      | IBM424                                      |
| IBM437                                      | IBM437                                      |
| IBM500                                      | IBM500                                      |
| IBM775                                      | IBM775                                      |
| IBM850                                      | IBM850                                      |
| IBM851                                      | IBM851                                      |
| IBM852                                      | IBM852                                      |
| IBM855                                      | IBM855                                      |
| IBM857                                      | IBM857                                      |
| IBM860                                      | IBM860                                      |
| IBM861                                      | IBM861                                      |
| IBM862                                      | IBM862                                      |
| IBM863                                      | IBM863                                      |
| IBM864                                      | IBM864                                      |
| IBM865                                      | IBM865                                      |
| IBM866                                      | IBM866                                      |
| IBM868                                      | IBM868                                      |
| IBM869                                      | IBM869                                      |
| IBM870                                      | IBM870                                      |
| IBM871                                      | IBM871                                      |
| IBM880                                      | IBM880                                      |
| IBM891                                      | IBM891                                      |
| IBM903                                      | IBM903                                      |
| IBM904                                      | IBM904                                      |
| IBM905                                      | IBM905                                      |
| IBM918                                      | IBM918                                      |
| IEC_P27-1                                   | IEC_P27_1                                   |
| INIS                                        | INIS                                        |
| INIS-8                                      | INIS_8                                      |
| INIS-cyrillic                               | INIS_CYRILLIC                               |
| INVARIANT                                   | INVARIANT                                   |
| ISO-10646-J-1                               | ISO_10646_J_1                               |
| ISO-10646-UCS-2                             | ISO_10646_UCS_2                             |
| ISO-10646-UCS-4                             | ISO_10646_UCS_4                             |
| ISO-10646-UCS-Basic                         | ISO_10646_UCS_BASIC                         |
| ISO-10646-UTF-1                             | ISO_10646_UTF_1                             |
| ISO-10646-Unicode-Latin1                    | ISO_10646_UNICODE_LATIN1                    |
| ISO-11548-1                                 | ISO_11548_1                                 |
| ISO-2022-CN                                 | ISO_2022_CN                                 |
| ISO-2022-CN-EXT                             | ISO_2022_CN_EXT                             |
| ISO-2022-JP                                 | ISO_2022_JP                                 |
| ISO-2022-JP-2                               | ISO_2022_JP_2                               |
| ISO-2022-KR                                 | ISO_2022_KR                                 |
| ISO-8859-1                                  | ISO_8859_1                                  |
| ISO-8859-1-Windows-3.0-Latin-1              | ISO_8859_1_WINDOWS_3_0_LATIN_1              |
| ISO-8859-1-Windows-3.1-Latin-1              | ISO_8859_1_WINDOWS_3_1_LATIN_1              |
| ISO-8859-10                                 | ISO_8859_10                                 |
| ISO-8859-13                                 | ISO_8859_13                                 |
| ISO-8859-14                                 | ISO_8859_14                                 |
| ISO-8859-15                                 | ISO_8859_15                                 |
| ISO-8859-16                                 | ISO_8859_16                                 |
| ISO-8859-2                                  | ISO_8859_2                                  |
| ISO-8859-2-Windows-Latin-2                  | ISO_8859_2_WINDOWS_LATIN_2                  |
| ISO-8859-3                                  | ISO_8859_3                                  |
| ISO-8859-4                                  | ISO_8859_4                                  |
| ISO-8859-5                                  | ISO_8859_5                                  |
| ISO-8859-6                                  | ISO_8859_6                                  |
| ISO-8859-6-E                                | ISO_8859_6_E                                |
| ISO-8859-6-I                                | ISO_8859_6_I                                |
| ISO-8859-7                                  | ISO_8859_7                                  |
| ISO-8859-8                                  | ISO_8859_8                                  |
| ISO-8859-8-E                                | ISO_8859_8_E                                |
| ISO-8859-8-I                                | ISO_8859_8_I                                |
| ISO-8859-9                                  | ISO_8859_9                                  |
| ISO-8859-9-Windows-Latin-5                  | ISO_8859_9_WINDOWS_LATIN_5                  |
| ISO-Unicode-IBM-1261                        | ISO_UNICODE_IBM_1261                        |
| ISO-Unicode-IBM-1264                        | ISO_UNICODE_IBM_1264                        |
| ISO-Unicode-IBM-1265                        | ISO_UNICODE_IBM_1265                        |
| ISO-Unicode-IBM-1268                        | ISO_UNICODE_IBM_1268                        |
| ISO-Unicode-IBM-1276                        | ISO_UNICODE_IBM_1276                        |
| ISO_10367-box                               | ISO_10367_BOX                               |
| ISO_2033-1983                               | ISO_2033_1983                               |
| ISO_5427                                    | ISO_5427                                    |
| ISO_5427:1981                               | ISO_5427_1981                               |
| ISO_5428:1980                               | ISO_5428_1980                               |
| ISO_646.basic:1983                          | ISO_646_BASIC_1983                          |
| ISO_646.irv:1983                            | ISO_646_IRV_1983                            |
| ISO_6937-2-25                               | ISO_6937_2_25                               |
| ISO_6937-2-add                              | ISO_6937_2_ADD                              |
| ISO_8859-supp                               | ISO_8859_SUPP                               |
| IT                                          | IT                                          |
| JIS_C6220-1969-jp                           | JIS_C6220_1969_JP                           |
| JIS_C6220-1969-ro                           | JIS_C6220_1969_RO                           |
| JIS_C6226-1978                              | JIS_C6226_1978                              |
| JIS_C6226-1983                              | JIS_C6226_1983                              |
| JIS_C6229-1984-a                            | JIS_C6229_1984_A                            |
| JIS_C6229-1984-b                            | JIS_C6229_1984_B                            |
| JIS_C6229-1984-b-add                        | JIS_C6229_1984_B_ADD                        |
| JIS_C6229-1984-hand                         | JIS_C6229_1984_HAND                         |
| JIS_C6229-1984-hand-add                     | JIS_C6229_1984_HAND_ADD                     |
| JIS_C6229-1984-kana                         | JIS_C6229_1984_KANA                         |
| JIS_Encoding                                | JIS_ENCODING                                |
| JIS_X0201                                   | JIS_X0201                                   |
| JIS_X0212-1990                              | JIS_X0212_1990                              |
| JUS_I.B1.002                                | JUS_I_B1_002                                |
| JUS_I.B1.003-mac                            | JUS_I_B1_003_MAC                            |
| JUS_I.B1.003-serb                           | JUS_I_B1_003_SERB                           |
| KOI7-switched                               | KOI7_SWITCHED                               |
| KOI8-R                                      | KOI8_R                                      |
| KOI8-U                                      | KOI8_U                                      |
| KSC5636                                     | KSC5636                                     |
| KS_C_5601-1987                              | KS_C_5601_1987                              |
| KZ-1048                                     | KZ_1048                                     |
| Latin-greek-1                               | LATIN_GREEK_1                               |
| MNEM                                        | MNEM                                        |
| MNEMONIC                                    | MNEMONIC                                    |
| MSZ_7795.3                                  | MSZ_7795_3                                  |
| Microsoft-Publishing                        | MICROSOFT_PUBLISHING                        |
| NATS-DANO                                   | NATS_DANO                                   |
| NATS-DANO-ADD                               | NATS_DANO_ADD                               |
| NATS-SEFI                                   | NATS_SEFI                                   |
| NATS-SEFI-ADD                               | NATS_SEFI_ADD                               |
| NC_NC00-10:81                               | NC_NC00_10_81                               |
| NF_Z_62-010                                 | NF_Z_62_010                                 |
| NF_Z_62-010_(1973)                          | NF_Z_62_010__1973_                          |
| NS_4551-1                                   | NS_4551_1                                   |
| NS_4551-2                                   | NS_4551_2                                   |
| OSD_EBCDIC_DF03_IRV                         | OSD_EBCDIC_DF03_IRV                         |
| OSD_EBCDIC_DF04_1                           | OSD_EBCDIC_DF04_1                           |
| OSD_EBCDIC_DF04_15                          | OSD_EBCDIC_DF04_15                          |
| PC8-Danish-Norwegian                        | PC8_DANISH_NORWEGIAN                        |
| PC8-Turkish                                 | PC8_TURKISH                                 |
| PT                                          | PT                                          |
| PT2                                         | PT2                                         |
| PTCP154                                     | PTCP154                                     |
| SCSU                                        | SCSU                                        |
| SEN_850200_B                                | SEN_850200_B                                |
| SEN_850200_C                                | SEN_850200_C                                |
| Shift_JIS                                   | SHIFT_JIS                                   |
| T.101-G2                                    | T_101_G2                                    |
| T.61-7bit                                   | T_61_7BIT                                   |
| T.61-8bit                                   | T_61_8BIT                                   |
| TIS-620                                     | TIS_620                                     |
| TSCII                                       | TSCII                                       |
| UNICODE-1-1                                 | UNICODE_1_1                                 |
| UNICODE-1-1-UTF-7                           | UNICODE_1_1_UTF_7                           |
| UNKNOWN-8BIT                                | UNKNOWN_8BIT                                |
| US-ASCII                                    | US_ASCII                                    |
| UTF-16                                      | UTF_16                                      |
| UTF-16BE                                    | UTF_16BE                                    |
| UTF-16LE                                    | UTF_16LE                                    |
| UTF-32                                      | UTF_32                                      |
| UTF-32BE                                    | UTF_32BE                                    |
| UTF-32LE                                    | UTF_32LE                                    |
| UTF-7                                       | UTF_7                                       |
| UTF-8                                       | UTF_8                                       |
| VIQR                                        | VIQR                                        |
| VISCII                                      | VISCII                                      |
| Ventura-International                       | VENTURA_INTERNATIONAL                       |
| Ventura-Math                                | VENTURA_MATH                                |
| Ventura-US                                  | VENTURA_US                                  |
| Windows-31J                                 | WINDOWS_31J                                 |
| dk-us                                       | DK_US                                       |
| greek-ccitt                                 | GREEK_CCITT                                 |
| greek7                                      | GREEK7                                      |
| greek7-old                                  | GREEK7_OLD                                  |
| hp-roman8                                   | HP_ROMAN8                                   |
| iso-ir-90                                   | ISO_IR_90                                   |
| latin-greek                                 | LATIN_GREEK                                 |
| latin-lap                                   | LATIN_LAP                                   |
| macintosh                                   | MACINTOSH                                   |
| us-dk                                       | US_DK                                       |
| videotex-suppl                              | VIDEOTEX_SUPPL                              |
| windows-1250                                | WINDOWS_1250                                |
| windows-1251                                | WINDOWS_1251                                |
| windows-1252                                | WINDOWS_1252                                |
| windows-1253                                | WINDOWS_1253                                |
| windows-1254                                | WINDOWS_1254                                |
| windows-1255                                | WINDOWS_1255                                |
| windows-1256                                | WINDOWS_1256                                |
| windows-1257                                | WINDOWS_1257                                |
| windows-1258                                | WINDOWS_1258                                |
| windows-874                                 | WINDOWS_874                                 |




## License

MIT (See [license file](LICENSE)).
